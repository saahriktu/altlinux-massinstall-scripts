#!/bin/bash
#
# exportrpmslistalt.sh v0.2
# bash script for exporting list of installed packages
# 2021 © Kurashov A.K., under GNU GPLv3

rpm -qa | rev | cut -d- -f 3- | rev > rpmsstate-$(date +%Y-%m-%d-%H-%M).list
