#!/bin/bash
#
# altpkglist2installscript.sh v0.2
# bash script for converting packages list to install script (with cleaning)
# 2021 © Kurashov A.K., under GNU GPLv3

if [ "$#" -lt 1 ]; then
        echo "usage: altpkglist2installscript.sh /path/to/file"
        exit
fi

APTPKGSLIST=""
MYTIMESTAMP=$(date +%Y-%m-%d-%H-%M)
while read -r IPKGNAME; do
    apt-cache show "$IPKGNAME" > /dev/null
    pkgps=$?
    if [ $pkgps -eq 0 ]; then
	APTPKGSLIST+=" $IPKGNAME"
    fi
done < "$1"
echo -e "\x23\x21/bin/bash\napt-get install$APTPKGSLIST" \
     > importrpmsstate-"${MYTIMESTAMP}".sh
chmod +x importrpmsstate-"${MYTIMESTAMP}".sh
