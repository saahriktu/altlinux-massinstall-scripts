#!/bin/bash
#
# altmassinstall.sh v0.2
# bash script for mass install packages in ALT linux from list
# 2021 © Kurashov A.K., under GNU GPLv3

if [ "$#" -lt 1 ]; then
        echo "usage: altmassinstall.sh /path/to/file"
        exit
fi

APTPKGSLIST=""
while read -r IPKGNAME; do
    apt-cache show "$IPKGNAME" > /dev/null
    pkgps=$?
    if [ $pkgps -eq 0 ]; then
	APTPKGSLIST+=" $IPKGNAME"
    fi
done < "$1"
apt-get install $APTPKGSLIST
